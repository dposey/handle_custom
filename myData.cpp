#include <inc/myData.h>

DBus::MessageIterator &operator>>(DBus::MessageIterator& i, struct _myData& f)
{
//  // Robert's first suggestion to Nick
//  f.octet = i.get_uint8();
//  i.next();
//  f.dword = i.get_uint32();
//  i.next();
//  f.deviceName = i.get_string();

  // Jose's method
  DBus::MessageIterator subiter = i.recurse();
  f.octet = subiter.get_uint8();
  subiter.next();
  f.dword = subiter.get_uint32();
  subiter.next();
  f.deviceName = subiter.get_string();

  return i;
}

DBus::MessageAppendIterator& operator<<(DBus::MessageAppendIterator& i, const struct _myData& f)
{
  i.append((uint8_t)f.octet);
  i.append((uint32_t)f.dword);
  i.append((std::string)f.deviceName);
  return i;
}
